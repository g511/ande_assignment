
import java.util.Scanner;

/**
 * @ClassName:
 * @Description:
 * @author Patrick CS1901-1916040110
 * @date 2020/11/15
 */
public class CMUtility {
private static Scanner scanner = new Scanner(System.in);
    
	//Get the selection input of the main menu
    public static char readMenuSelection() 
	{
        char c;
        for (; ; ) 
        {
            String str = readKeyBoard(1, false);
            c = str.charAt(0);
            if (c != '1' && c != '2' && c != '3' && c != '4' && c != '5'&& c != '6'&& c != '7') 
            {
                System.out.print("Selection error, please re-enter：");
            }
            else if(str.length()>=2){
            	System.out.print("Selection error, please re-enter：");
            }else{
            	return c;
            }
            	
        }
    }

    //Get the first character input
    public static char readChar() 
    {
        String str = readKeyBoard(1, false);
        
        return str.charAt(0);
    }

    //Get character input and compare with default value
    public static char readChar(char defaultValue) 
    {
        String str = readKeyBoard(1, true);
        
        return (str.length() == 0) ? defaultValue : str.charAt(0);
    }

    //Get the first 2 digital inputs
    public static int readInt() 
    {
        int n;
        for (; ; ) 
        {
            String str = readKeyBoard(2, false);
            try 
            {
                n = Integer.parseInt(str);
                break;
            } 
            catch (NumberFormatException e) 
            {
                System.out.print("Number input error, please re-enter：");
            }
        }
        
        return n;
    }

    //Get digital input and compare with default value
    public static int readInt(int defaultValue) 
    {
        int n;
        for (; ; ) 
        {
            String str = readKeyBoard(2, true);
            if (str.equals("")) 
            {
                return defaultValue;
            }

            try 
            {
                n = Integer.parseInt(str);
                break;
            } 
            catch (NumberFormatException e) 
            {
                System.out.print("Number input error, please re-enter：");
            }
        }
        
        return n;
    }

    //Get a string input of a specified length
    public static String readString(int limit) 
    {
        return readKeyBoard(limit, false);
    }

    //Get the specified length string input and compare with the default value
    public static String readString(int limit, String defaultValue) 
    {
        String str = readKeyBoard(limit, true);
        return str.equals("")? defaultValue : str;
    }

    //Get y or n character input
    public static char readConfirmSelection() 
    {
        char c;
        for (; ; ) 
        {
            String str = readKeyBoard(1, false).toUpperCase();
            c = str.charAt(0);
            if (c == 'Y' || c == 'N') 
            {
                break;
            } 
            else 
            {
                System.out.print("Selection error, please re-enter：");
            }
        }
        
        return c;
    }

    //Get input of specified length, if blankReturn is false, continue to get input
    private static String readKeyBoard(int limit, boolean blankReturn) 
    {
        String line = "";

        while (scanner.hasNextLine()) 
        {
            line = scanner.nextLine();
            
            //If the input is empty, judge blankReturn
            if (line.length() == 0) 
            {
                //Can be empty to return, otherwise continue to wait for input
            	if (blankReturn) 
                	return line;
                else 
                	continue;
            }

            /*//If the input does not meet the length requirement
            if (line.length() < 1 || line.length() > limit) 
            {
                System.out.print("The input length (not greater than "+ limit + ") is wrong, please re-enter：");
                continue;
            }*/
            
            //Input meets the requirements
            break;
        }

        return line;
    }
}
